console.log("Hello world");

// JavaScript renders web pages in an interactive and dynamic fashion. Meaning, it enables us to create dynamically updating content, control multimedia, and animate images.

// Syntax, statements and comments

// Statements:

// Statements in programming are instructions that we tell the computer to perform
// JS statements usually end with a semicolon (;)
// Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends.

// Syntax in programming is the set of rules that describes how statements must be formed or constructed

// Comments are parts of the code that get ignored by the language
// Comments are meant to describe the written code

/*
  There are two types of comments:
  1. The single-line comment //
  
  2. The multi-line comment, slash asterisk
*/

// Variables

// Used to contain data
// Makes it easy to associate information in our devices to actual names about information

// Declaring variables
// declaring variables tells us our devices that a variable name is create and is ready to store data

let myVariable;
console.log(myVariable);

// Declaring a variable without giving it a value will automatically assign it with the value of "undefined"

// Syntax
// let/const variableName;

// console.log() is useful for printing values of variables or certain results of code into the browser's console
// constant use of this through out developing an application will save us time and builds good habits in always checking for the output of our code.

let hello;
console.log(hello);

// Variables mus be declared before they are used
// using variables before they are declared will result with an error

/*
  Guides in writing variables
    1. Use the 'let' keyword followed by the variable name and use the assignment operator (=) to assign a value.
    2. Variable names should start with a lowercase character, use camelCase 
    3. For constant variables, use the 'const' keyword
    4. Variable names should be descriptive of the value being stored to avoid confusion
*/

// Declare and initialize variables
//  Initializing variables - the instance when a variable is given its initial value
// Syntax: 
//  let/const variableName = value;

let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
//const pi = 3.1416;

// Reassigning variable values - changing its previous value
productName = "laptop";
console.log(productName);

//let friend = 'Kate';
//let friend = 'Nej'; // error
//friend = 'Nej';

//interest = 1; // error

// Reassigning vs initializing variables

let supplier;
// Initialization - first value
supplier = 'John Smith Trading';
console.log(supplier);

// Reassignment
supplier = 'Zuitt Store';
console.log(supplier);

// const pi; // error missing initialization
// pi = 3.1416;
// console.log(pi);

// var
// also used to declare variables

// Hoisting - error prone
//a = 5;
//console.log(a);
//var a;

// Hoisting is JS behavior of moving declarations to the top

//a = 5; // Error
//console.log(a)
//let a;

// let/const local/global scope
// block scope - within curly braces

let outerVariable = 'Hlelo';

{
  let innerVariable = "Hello again";
}

console.log(outerVariable);
//console.log(innerVariable); // Error

// Multiple variable declarations
//  multiple vars can be declared in one line
// quicker, but against best practice, less readable

//let productCode = "DC017", productBrand = "Dell";
let productCode = "DC017";
const productBrand = "Dell";

console.log(productCode, productBrand);

// Using a variable with a reserved keyword
//const let = "hello"; // Error

// Data types

// String
// series of characters, either single our double quotes

let country = "Philippines";
let province = "Metro Manila";

// Concatenating strings
// using "+"

let fullAddress = province + ", " + country
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// The escape character
// "\n" newline

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);
// Metro Manila
//
// Philippines

// Using the double quotes along with single quotes can allow us to easily include single quotes in text without using the escape character.

let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

// Numbers

// Integers/Whole numbers
let headcount = 26;
console.log(headcount); // 26

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade); // 98.7

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance); // 20000000000

// Combining text and strings
console.log("Johns grade last qurater is " + grade);
// John's grade last quarter is 98.7

// Boolean - true/false

let isMarried = false;
let inGoodConduct = true;

console.log("is Married: " + isMarried);
console.log("is in good conduct: " + inGoodConduct);

// Arrays
// Can store different data types
// Array literals

let grades = [92.1, 90.2, 88, 72];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// another special kind of data type that is used to mimic real world object
// Used to create complex data that contain pieces of relevant information

let person = {
  fullName: "Edward Scissorhands",
  age: 25,
  isMarried: false,
  contact: ["+639171234567", "8123 4567"],
  address: {
    houseNumber: "345",
    city: "Manila"
  }
}

console.log(person);

let myGrades = {
  firstGrading: 98.1,
  secondGrading: 99.2,
  thirdGrading: 92.1,
  fourthGrading: 92.2
}
console.log(myGrades);

console.log(typeof myGrades);
console.log(typeof grades);
// note: array is a special type of object with methods and functions to manipulate data

const anime = ["OP", "OPM", "AOT", "BNHA"];

anime[0] = "JJK";
console.log(anime);

// Null
let spouse = null;
console.log(spouse);
// null is also considered a data type of its own compared to 0 which is a number and single quotes which are a data type of a string

let myNumber = 0;
let myString = '';
console.log(myNumber);
console.log(myString);

// Undefined
let fullName;
console.log(fullName); // undefined

// one clear difference between undefined and null is that for undefined, a variable was created but was not provided with a value.
